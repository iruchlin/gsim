import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

@SuppressWarnings("serial")
public class Control extends JPanel implements ActionListener, ItemListener {

	private static JButton runPauseButton;
	private static JButton undoButton;
	private static JButton resetButton;
	private static JButton COMButton;
	private static JButton plotResetButton;
	private static JCheckBox gravityToggle;
	private static JCheckBox LJToggle;
	private static JCheckBox elasticToggle;
	private static JCheckBox uniformGToggle;
	private static JCheckBox HToggle;
	private static JCheckBox PxToggle;
	private static JCheckBox PyToggle;
	private static JCheckBox JzToggle;
	private static JRadioButton noneBoundaryRadio;
	private static JRadioButton reflectionBoundaryRadio;
	private static JRadioButton torusBoundaryRadio;
	private static JRadioButton sphereBoundaryRadio;
	private static ButtonGroup boundaryRadioGroup;
	private static JSlider massSlider;
	private static JComboBox<?> integratorSwitch;
	private static JButton openButton;
	private static JButton saveButton;
	private static JButton addBodyButton;
	public static JTextField newBody_rx;
	public static JTextField newBody_ry;
	public static JTextField newBody_vx;
	public static JTextField newBody_vy;
	private static JSpinner delaySpinner;
	private static JSpinner windowSpinner;
	private static JSpinner dtSpinner;
	
	private static JTabbedPane tabbedPane;
	
	public Control() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		Border bev_border = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		
		TitledBorder titled = BorderFactory.createTitledBorder(bev_border, "Simulation");
		
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setBorder(titled);
		buttonsPanel.setLayout(new GridLayout(0, 2));
		
		runPauseButton = new JButton("Run");
		runPauseButton.setToolTipText("Run or pause the simulation");
		runPauseButton.setActionCommand("runPause");
		runPauseButton.addActionListener(this);
		buttonsPanel.add(runPauseButton);
		
		undoButton = new JButton("Undo");
		undoButton.setToolTipText("Undo most recently added body");
		undoButton.setActionCommand("Undo");
		undoButton.addActionListener(this);
		buttonsPanel.add(undoButton);
		
		resetButton = new JButton("Reset");
		resetButton.setToolTipText("Reset the simulation to empty");
		resetButton.setActionCommand("resetNbodies");
		resetButton.addActionListener(this);
		buttonsPanel.add(resetButton);
		
		COMButton = new JButton("COM");
		COMButton.setToolTipText("Enter center-of-mass/center-of-momentum frame");
		COMButton.setActionCommand("COM");
		COMButton.addActionListener(this);
		buttonsPanel.add(COMButton);
		
		titled = BorderFactory.createTitledBorder(bev_border, "Interactions");
		
		JPanel interactionsPanel = new JPanel();
		interactionsPanel.setBorder(titled);
		interactionsPanel.setLayout(new GridLayout(0, 2));
		
		gravityToggle = new JCheckBox("G");
		gravityToggle.setToolTipText("Newtonian gravitational potential");
		gravityToggle.setSelected(true);
		gravityToggle.addItemListener(this);
		interactionsPanel.add(gravityToggle);
		
		LJToggle = new JCheckBox("LJ");
		LJToggle.setToolTipText("Lennard-Jones molecular potential");
		LJToggle.setSelected(false);
		LJToggle.addItemListener(this);
		interactionsPanel.add(LJToggle);
		
		elasticToggle = new JCheckBox("E");
		elasticToggle.setToolTipText("Elastic collision");
		elasticToggle.setSelected(true);
		elasticToggle.addItemListener(this);
		interactionsPanel.add(elasticToggle);
		
		uniformGToggle = new JCheckBox("g");
		uniformGToggle.setToolTipText("Uniform downward acceleration");
		uniformGToggle.setSelected(false);
		uniformGToggle.addItemListener(this);
		interactionsPanel.add(uniformGToggle);
		
		titled = BorderFactory.createTitledBorder(bev_border, "Boundary");
		
		JPanel boundaryPanel = new JPanel();
		boundaryPanel.setBorder(titled);
		boundaryPanel.setLayout(new GridLayout(0, 2));
		
		noneBoundaryRadio = new JRadioButton("None");
		noneBoundaryRadio.setToolTipText("No boundary");
		noneBoundaryRadio.setActionCommand("noneBoundary");
		noneBoundaryRadio.addActionListener(this);
		noneBoundaryRadio.setSelected(true);
		boundaryPanel.add(noneBoundaryRadio);
		
		reflectionBoundaryRadio = new JRadioButton("Reflection");
		reflectionBoundaryRadio.setToolTipText("Elastic boundary reflection");
		reflectionBoundaryRadio.setActionCommand("reflectionBoundary");
		reflectionBoundaryRadio.addActionListener(this);
		boundaryPanel.add(reflectionBoundaryRadio);

		torusBoundaryRadio = new JRadioButton("Torus");
		torusBoundaryRadio.setToolTipText("Left-right and top-bottom identification");
		torusBoundaryRadio.setActionCommand("torusBoundary");
		torusBoundaryRadio.addActionListener(this);
		boundaryPanel.add(torusBoundaryRadio);
		
		sphereBoundaryRadio = new JRadioButton("Sphere");
		sphereBoundaryRadio.setToolTipText("Spherical surface");
		sphereBoundaryRadio.setActionCommand("sphereBoundary");
		sphereBoundaryRadio.addActionListener(this);
		boundaryPanel.add(sphereBoundaryRadio);
		
		boundaryRadioGroup = new ButtonGroup();
		boundaryRadioGroup.add(noneBoundaryRadio);
		boundaryRadioGroup.add(reflectionBoundaryRadio);
		boundaryRadioGroup.add(torusBoundaryRadio);
		boundaryRadioGroup.add(sphereBoundaryRadio);
		
		titled = BorderFactory.createTitledBorder(bev_border, "Add New Body");
		
		JPanel newBodyPanel = new JPanel();
		newBodyPanel.setBorder(titled);
		newBodyPanel.setLayout(new GridLayout(0, 1));
		
		titled = BorderFactory.createTitledBorder("Position");
		
		JPanel positionPanel = new JPanel();
		positionPanel.setBorder(titled);
		positionPanel.setLayout(new GridLayout(2, 2));
		
		newBody_rx = new JTextField("0.0");
		newBody_ry = new JTextField("0.0");
		
		positionPanel.add(new JLabel("rx"));
		positionPanel.add(newBody_rx);
		positionPanel.add(new JLabel("ry"));
		positionPanel.add(newBody_ry);
		
		newBodyPanel.add(positionPanel);
		
		titled = BorderFactory.createTitledBorder("Velocity");
		
		JPanel velocityPanel = new JPanel();
		velocityPanel.setBorder(titled);
		velocityPanel.setLayout(new GridLayout(2, 2));
		
		newBody_vx = new JTextField("0.0");
		newBody_vy = new JTextField("0.0");
		
		velocityPanel.add(new JLabel("vx"));
		velocityPanel.add(newBody_vx);
		velocityPanel.add(new JLabel("vy"));
		velocityPanel.add(newBody_vy);
		
		newBodyPanel.add(velocityPanel);
		
		titled = BorderFactory.createTitledBorder("Log10 Mass");
		
		JPanel massSliderPanel = new JPanel();
		massSliderPanel.setBorder(titled);
		massSliderPanel.setLayout(new GridLayout(0, 1));
		
		massSlider = new JSlider(JSlider.HORIZONTAL, 0, 8, 4);
		massSlider.setToolTipText("Set the order of magnitude of the next body's mass");
		massSlider.setMinorTickSpacing(1);
		massSlider.setMajorTickSpacing(2);
		massSlider.setPaintTicks(true);
		massSlider.setPaintLabels(true);
		massSlider.setName("Mass");
		massSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider)e.getSource();
			
				if(!source.getValueIsAdjusting()) {
					Stage.mass = Math.pow(10.0, source.getValue());
				}
			}
		});
		
		
		massSliderPanel.add(massSlider);
		
		newBodyPanel.add(massSliderPanel);
		
		addBodyButton = new JButton("Add Body");
		addBodyButton.setToolTipText("Add a new body with the above position, velocity, and mass parameters");
		addBodyButton.setActionCommand("addBody");
		addBodyButton.addActionListener(this);
		newBodyPanel.add(addBodyButton);
		
		titled = BorderFactory.createTitledBorder(bev_border, "State I/O");
		
		JPanel stateIOPanel = new JPanel();
		stateIOPanel.setBorder(titled);
		stateIOPanel.setLayout(new GridLayout(0, 2));
		
		openButton = new JButton("Open");
		openButton.setToolTipText("Open GSim state");
		openButton.setActionCommand("Open");
		openButton.addActionListener(this);
		stateIOPanel.add(openButton);
		
		saveButton = new JButton("Save");
		saveButton.setToolTipText("Save GSim state");
		saveButton.setActionCommand("Save");
		saveButton.addActionListener(this);
		stateIOPanel.add(saveButton);
		
		JPanel statePanel = new JPanel();
		statePanel.setLayout(new BoxLayout(statePanel, BoxLayout.Y_AXIS));
		statePanel.add(buttonsPanel);
		statePanel.add(interactionsPanel);
		statePanel.add(boundaryPanel);
		statePanel.add(newBodyPanel);
		statePanel.add(stateIOPanel);
		
		titled = BorderFactory.createTitledBorder(bev_border, "Rendering");
		
		JPanel plottingRenderingPanel = new JPanel();
		plottingRenderingPanel.setBorder(titled);
		plottingRenderingPanel.setLayout(new GridLayout(0, 2));
		
		delaySpinner = new JSpinner(new SpinnerNumberModel(500, 25, null, 1));
		delaySpinner.setToolTipText("Time in milliseconds between plotting updates");
		delaySpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				Plotting.DELAY = (int)((JSpinner)e.getSource()).getValue();
			}
		});
		plottingRenderingPanel.add(new JLabel("Interval [ms]"));
		plottingRenderingPanel.add(delaySpinner);
		
		windowSpinner = new JSpinner(new SpinnerNumberModel(25, 0, null, 1));
		windowSpinner.setToolTipText("Width of domain before current time; set to 0 to show entire domain");
		windowSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				Plotting.twindow = (int)((JSpinner)e.getSource()).getValue();
				System.out.println("Plotting.twindow = " + Plotting.twindow);
			}
		});
		plottingRenderingPanel.add(new JLabel("Window"));
		plottingRenderingPanel.add(windowSpinner);
		
		///////////////
		titled = BorderFactory.createTitledBorder(bev_border, "Plot");
		
		JPanel plotPanel = new JPanel();
		plotPanel.setBorder(titled);
		plotPanel.setLayout(new GridLayout(0, 2));
		
		HToggle = new JCheckBox("H");
		HToggle.setToolTipText("Hamiltonian");
		HToggle.setSelected(true);
		HToggle.addItemListener(this);
		plotPanel.add(HToggle);
		
		PxToggle = new JCheckBox("Px");
		PxToggle.setToolTipText("Linear x-momentum");
		PxToggle.setSelected(true);
		PxToggle.addItemListener(this);
		plotPanel.add(PxToggle);
		
		PyToggle = new JCheckBox("Py");
		PyToggle.setToolTipText("Linear y-momentum");
		PyToggle.setSelected(true);
		PyToggle.addItemListener(this);
		plotPanel.add(PyToggle);
		
		JzToggle = new JCheckBox("Jz");
		JzToggle.setToolTipText("Angular momentum");
		JzToggle.setSelected(true);
		JzToggle.addItemListener(this);
		plotPanel.add(JzToggle);
		
		plotResetButton = new JButton("Reset");
		plotResetButton.setToolTipText("Reset the plot");
		plotResetButton.setActionCommand("resetPlot");
		plotResetButton.addActionListener(this);
		plotPanel.add(plotResetButton);
		///////////////
		
		JPanel plottingPanel = new JPanel();
		plottingPanel.setLayout(new BoxLayout(plottingPanel, BoxLayout.Y_AXIS));
		plottingPanel.add(plottingRenderingPanel);
		plottingPanel.add(plotPanel);
		
		titled = BorderFactory.createTitledBorder(bev_border, "Integration");
		
		JPanel integrationPanel = new JPanel();
		integrationPanel.setBorder(titled);
		integrationPanel.setLayout(new GridLayout(4, 1));
		
		String[] integrators = {"Stormer-Verlet 2", "Stormer-Verlet 4", "Runge-Kutta 2", "Runge-Kutta 4"};
		integratorSwitch = new JComboBox<Object>(integrators);
		integratorSwitch.setToolTipText("Select the integrator method type and accuracy order");
		integratorSwitch.setSelectedIndex(1);
		integratorSwitch.setEditable(false);
		integratorSwitch.setActionCommand("switchIntegrator");
		integratorSwitch.addActionListener(this);
		integrationPanel.add(integratorSwitch);
		
		dtSpinner = new JSpinner(new SpinnerNumberModel(0.01, 0.001, null, 0.001));
		dtSpinner.setToolTipText("(small, accurate)<--- dt --->(large, fast)");
		dtSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				Stage.dt = (double)((JSpinner)e.getSource()).getValue();
			}
		});
		
		titled = BorderFactory.createTitledBorder("Time Step");
		
		JPanel timeStepPanel = new JPanel();
		timeStepPanel.setBorder(titled);
		timeStepPanel.setLayout(new GridLayout(0, 1));
		timeStepPanel.add(dtSpinner);
		integrationPanel.add(timeStepPanel);
		
		JPanel advancedPanel = new JPanel();
		advancedPanel.setLayout(new BoxLayout(advancedPanel, BoxLayout.Y_AXIS));
		advancedPanel.add(integrationPanel);
		
		tabbedPane = new JTabbedPane();
		tabbedPane.addTab("State", statePanel);
		tabbedPane.addTab("Plotting", plottingPanel);
		tabbedPane.addTab("Advanced", advancedPanel);
		
		add(tabbedPane);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if("resetNbodies".equals(e.getActionCommand())) {
			Stage.Body.resetBodies();
			newBody_rx.setText("0.0");
			newBody_ry.setText("0.0");
			newBody_vx.setText("0.0");
			newBody_vy.setText("0.0");
		}
		if("Undo".equals(e.getActionCommand())) {
			if(Stage.Nbodies > 0) {
				Stage.bodies[Stage.Nbodies - 1] = null;
				Stage.Nbodies--;
			}
			Stage.Body.recalc_Plotting_reference = true;
		}
		else if("runPause".equals(e.getActionCommand())) {
			Stage.run = !Stage.run;
			
			if(Stage.run) {
				runPauseButton.setText("Pause");
			}
			else {
				runPauseButton.setText("Run");
			}
		}
		else if("COM".equals(e.getActionCommand())) {
			Stage.Body.setCOMFrame();
			System.out.println("Entering center-of-mass/center-of-momentum frame.");
		}
		if("resetPlot".equals(e.getActionCommand())) {
			Plotting.reset_series = true;
		}
		else if("noneBoundary".equals(e.getActionCommand())) {
			Stage.boundary = "None";
		}
		else if("reflectionBoundary".equals(e.getActionCommand())) {
			Stage.boundary = "Reflection";
		}
		else if("torusBoundary".equals(e.getActionCommand())) {
			Stage.boundary = "Torus";
		}
		else if("sphereBoundary".equals(e.getActionCommand())) {
			Stage.boundary = "Sphere";
		}
		else if("addBody".equals(e.getActionCommand())) {
			final double rx = Double.parseDouble(newBody_rx.getText());
			final double ry = Double.parseDouble(newBody_ry.getText());
			final double vx = Double.parseDouble(newBody_vx.getText());
			final double vy = Double.parseDouble(newBody_vy.getText());
			
			Stage.Body.loadBody(Stage.mass, rx, ry, vx, vy);
			//Stage.Body.setCOMFrame();
		}
		else if("switchIntegrator".equals(e.getActionCommand())) {
			Stage.integrator = (String)((JComboBox<?>)e.getSource()).getSelectedItem();
		}
		else if("Open".equals(e.getActionCommand())) {
			JFileChooser fileChooser = new JFileChooser(new File("states/").getAbsolutePath());
			fileChooser.setFileFilter(new FileNameExtensionFilter("GSim State Files", "gsm"));
			if(fileChooser.showOpenDialog(getComponent(0)) == JFileChooser.APPROVE_OPTION) {
				
			  	Stage.Body.resetBodies();
			  	
				try {
					BufferedReader input = new BufferedReader(new FileReader(fileChooser.getSelectedFile()));
					
					String b;
					
					while((b = input.readLine()) != null) {
						String[] params = b.split(" ");
						Stage.Body.loadBody(Double.parseDouble(params[0]), Double.parseDouble(params[1]), Double.parseDouble(params[2]), Double.parseDouble(params[3]), Double.parseDouble(params[4]));
					}
					
					input.close();
				} catch (IOException e2) {e2.printStackTrace();}
				
			  	//Stage.Body.setCOMFrame();

				System.out.println("State opened...");
			}
		}
		else if("Save".equals(e.getActionCommand())) {
			JFileChooser fileChooser = new JFileChooser(new File("states/").getAbsolutePath()) {
				@Override
				public void approveSelection() {
					File f = getSelectedFile();
					if(f.exists() && getDialogType() == SAVE_DIALOG){
						final int result = JOptionPane.showConfirmDialog(this, "Overwrite existing file?", "Existing File", JOptionPane.YES_NO_CANCEL_OPTION);
						switch(result){
							case JOptionPane.YES_OPTION:
								super.approveSelection();
								return;
			                case JOptionPane.NO_OPTION:
			                    return;
			                case JOptionPane.CLOSED_OPTION:
			                    return;
			                case JOptionPane.CANCEL_OPTION:
			                    cancelSelection();
			                    return;
			            }
			        }
			        super.approveSelection();
				}
			};

			fileChooser.setFileFilter(new FileNameExtensionFilter("GSim State Files", "gsm"));
			
			if (fileChooser.showSaveDialog(getComponent(0)) == JFileChooser.APPROVE_OPTION) {
				try {
					FileWriter output = new FileWriter(fileChooser.getSelectedFile());

					for(int i = 0; i < Stage.Nbodies; i++) {
						output.write("" + Stage.bodies[i].m);
						output.write(" " + Stage.bodies[i].rxn);
						output.write(" " + Stage.bodies[i].ryn);
						output.write(" " + Stage.bodies[i].vxn);
						output.write(" " + Stage.bodies[i].vyn + "\n");
					}

					output.close();
				} catch (IOException e2) {e2.printStackTrace();}

				System.out.println("State saved...");
			}

		}
	}

	@Override
	public void itemStateChanged(ItemEvent arg0) {
		Object source = arg0.getItemSelectable();

		if(source == gravityToggle) {
			Stage.gravityOn = !Stage.gravityOn;
		}
		if(source == LJToggle) {
			Stage.LJOn = !Stage.LJOn;
		}
		else if(source == elasticToggle) {
			Stage.elasticOn = !Stage.elasticOn;
		}
		else if(source == uniformGToggle) {
			Stage.uniformGOn = !Stage.uniformGOn;
		}
		else if(source == HToggle) {
			Plotting.HOn = !Plotting.HOn;
		}
		else if(source == PxToggle) {
			Plotting.PxOn = !Plotting.PxOn;
		}
		else if(source == PyToggle) {
			Plotting.PyOn = !Plotting.PyOn;
		}
		else if(source == JzToggle) {
			Plotting.JzOn = !Plotting.JzOn;
		}
	}
	
}
