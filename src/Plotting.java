import java.awt.BasicStroke;
import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import org.jfree.data.xy.XYSeries;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeriesCollection;

@SuppressWarnings("serial")
public class Plotting extends JPanel implements Runnable {

	public static int DELAY = 500;
	public static double twindow = 25.0;
	
	private XYSeries series_H;
	private XYSeries series_Jz;
	private XYSeries series_Px;
	private XYSeries series_Py;
	private XYSeriesCollection dataset;
	private JFreeChart chart;
	private XYPlot plot;
	private ValueAxis domainAxis;
	private NumberAxis rangeAxis;
	private XYLineAndShapeRenderer renderer;
	private ChartPanel chartPanel;
	
	private Thread animator;
	
	public static boolean reset_series = true;
	
	public static double reference_Hamiltonian = 0.0;
	public static double reference_angular_momentum = 0.0;
	public static double reference_linear_momentum_x = 0.0;
	public static double reference_linear_momentum_y = 0.0;
	public static boolean HOn = true;
	public static boolean PxOn = true;
	public static boolean PyOn = true;
	public static boolean JzOn = true;
	
	public Plotting() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		series_H = new XYSeries("H");
		series_Px = new XYSeries("Px");
		series_Py = new XYSeries("Py");
		series_Jz = new XYSeries("Jz");
		
		dataset = new XYSeriesCollection();
		dataset.addSeries(series_H);
		dataset.addSeries(series_Px);
		dataset.addSeries(series_Py);
		dataset.addSeries(series_Jz);
		
		chart = ChartFactory.createXYLineChart("Conserved Quantities", "Time", "", dataset);
		chart.setBorderVisible(true);
		
		plot = chart.getXYPlot();
		renderer = new XYLineAndShapeRenderer();
		renderer.setSeriesPaint(0, Color.RED);
		renderer.setSeriesStroke(0, new BasicStroke(2.0f));
		plot.setRenderer(renderer);

		plot.setBackgroundPaint(Color.WHITE);
		plot.setDomainGridlinesVisible(true);
		plot.setDomainGridlinePaint(Color.BLACK);
		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(Color.BLACK);

		domainAxis = plot.getDomainAxis();
		rangeAxis = (NumberAxis)plot.getRangeAxis();
		
		rangeAxis.setAutoRangeIncludesZero(false);
		
		chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(300, 300));
		add(chartPanel);
	}

	@Override
    public void addNotify() {
        super.addNotify();

        animator = new Thread(this);
        animator.start();
    }
	
	private void updatePlots() {
		if(twindow == 0) {
			domainAxis.setRange(0.0, Stage.time);
		}
		else {
			domainAxis.setRange(Math.max(0.0, Stage.time - twindow), Stage.time);
		}
		
		final double EPS = 1.0e-16;
		
		if(HOn) {
			final double rel_err_H = Math.abs((Stage.Hamiltonian - reference_Hamiltonian) / (reference_Hamiltonian + EPS));
			series_H.add(Stage.time, Math.log10(rel_err_H + EPS));
		}
		
		if(PxOn) {
			final double abs_err_Px = Math.abs(Stage.linear_momentum_x - reference_linear_momentum_x + EPS);
			series_Px.add(Stage.time, Math.log10(abs_err_Px + EPS));
		}
		
		if(PyOn) {
			final double abs_err_Py = Math.abs(Stage.linear_momentum_y - reference_linear_momentum_y + EPS);
			series_Py.add(Stage.time, Math.log10(abs_err_Py + EPS));
		}
		
		if(JzOn) {
			final double rel_err_Jz = Math.abs((Stage.angular_momentum - reference_angular_momentum) / (reference_angular_momentum + EPS));
			series_Jz.add(Stage.time, Math.log10(rel_err_Jz + EPS));
		}
	}
	
	@Override
	public void run() {
		long beforeTime, timeDiff, sleep;
		
		beforeTime = System.currentTimeMillis();
		
		while(true) {

			if(Stage.run) {
				if(reset_series) {
					series_H.clear();
					series_Px.clear();
					series_Py.clear();
					series_Jz.clear();
					reset_series = false;
				}
				
				updatePlots();
			}
			
			repaint();
			
			timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = DELAY - timeDiff;

            if(sleep < 0) {
                sleep = 2;
            }

            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                System.out.println("Interrupted: " + e.getMessage());
            }

            beforeTime = System.currentTimeMillis();
		}
		
	}
	
}