import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Stage extends JPanel implements Runnable, MouseListener, MouseMotionListener {

	private static final int DELAY = 25;
	public static final int panelWidth = 600;
	public static final int panelHeight = 600;
	private static final int NMAX = 1024;
	private static final double velFac = 0.25;
	
	private static boolean cursorPressed = false;
	private static int cursorPressX, cursorPressY, cursorCurrentX, cursorCurrentY;
	
	private Thread animator;
	
	public static int Nbodies = 0;

	public static String integrator = "Stormer-Verlet 2";
	public static boolean run = false;
	public static boolean elasticOn = true;
	public static boolean gravityOn = true;
	public static boolean LJOn = false;
	public static boolean uniformGOn = false;

	public static String boundary = "None";
	
	public static double mass = 1000.0;

	public static double Hamiltonian = 0.0;
	public static double angular_momentum = 0.0;
	public static double linear_momentum_x = 0.0;
	public static double linear_momentum_y = 0.0;
	public static double boundary_momentum_x = 0.0;
	public static double boundary_momentum_y = 0.0;
	public static double time = 0.0;
	public static double dt = 0.01;
	
	public static Body bodies[] = new Body[NMAX];
	
	
	public Stage() {
		setBorder(BorderFactory.createLineBorder(Color.green));
		setBackground(Color.BLACK);
		//setOpaque(true);
		setPreferredSize(new Dimension(panelWidth, panelHeight));
		setDoubleBuffered(true);
		addMouseListener(this);
		addMouseMotionListener(this);
	}

	@Override
    public void addNotify() {
        super.addNotify();

        animator = new Thread(this);
        animator.start();
    }
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		cursorPressed = true;
		cursorPressX = arg0.getX();
		cursorPressY = arg0.getY();
		cursorCurrentX = cursorPressX;
		cursorCurrentY = cursorPressY;
		
		Control.newBody_rx.setText(Double.toString(cursorPressX));
		Control.newBody_ry.setText(Double.toString(cursorPressY));
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		cursorPressed = false;
		int cursorReleaseX = arg0.getX();
		int cursorReleaseY = arg0.getY();
		
		double vx = velFac * (cursorReleaseX - cursorPressX);
		double vy = velFac * (cursorReleaseY - cursorPressY);
		
		if(Nbodies < NMAX) {
			bodies[Nbodies] = new Body(mass, cursorPressX, cursorPressY, vx, vy);
		}
		
		Control.newBody_rx.setText("0.0");
		Control.newBody_ry.setText("0.0");
		Control.newBody_vx.setText("0.0");
		Control.newBody_vy.setText("0.0");
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		cursorCurrentX = e.getX();
		cursorCurrentY = e.getY();
		
		double vx = velFac * (cursorCurrentX - cursorPressX);
		double vy = velFac * (cursorCurrentY - cursorPressY);
		Control.newBody_vx.setText(Double.toString(vx));
		Control.newBody_vy.setText(Double.toString(vy));
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		cursorCurrentX = e.getX();
		cursorCurrentY = e.getY();
		
		Control.newBody_rx.setText(Double.toString(cursorCurrentX));
		Control.newBody_ry.setText(Double.toString(cursorCurrentY));
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for(int i = 0; i < Nbodies; i++) {
			g.setColor(bodies[i].color);
			double radius = bodies[i].radius;
			g.fillOval((int)(bodies[i].rxn - 0.5 * radius), (int)(bodies[i].ryn - 0.5 * radius), (int)(bodies[i].radius), (int)(bodies[i].radius));
		}
		
		
		if(cursorPressed) {
			g.setColor(Color.MAGENTA);
			g.drawLine(cursorPressX, cursorPressY, cursorCurrentX, cursorCurrentY);
		}
		
		g.setColor(Color.GREEN);
		int textField_rx = (int)Double.parseDouble(Control.newBody_rx.getText());
		int textField_ry = (int)Double.parseDouble(Control.newBody_ry.getText());
		g.drawLine(textField_rx - 5, textField_ry, textField_rx + 5, textField_ry);
		g.drawLine(textField_rx, textField_ry - 5, textField_rx, textField_ry + 5);
		
		Toolkit.getDefaultToolkit().sync();
	}
	
	@Override
	public void run() {
		long beforeTime, timeDiff, sleep;
		
		beforeTime = System.currentTimeMillis();
		
		while(true) {

			if(run) {
				Body.updateBodies();
			}
			
			repaint();
			
			timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = DELAY - timeDiff;

            if(sleep < 0) {
                sleep = 2;
            }

            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                System.out.println("Interrupted: " + e.getMessage());
            }

            beforeTime = System.currentTimeMillis();
		}
		
	}

	public static class Body {
		
		private static final double eps2 = 0.0;
		private static final double G = 1.0;
		private static final double LJ_mag = 1.0;
		public static boolean recalc_Plotting_reference = true;
		
		private int id;
		public double m;
		public double radius;
		public double rxn, ryn;
		public double vxn, vyn;
		public double axn, ayn;
		public static double potential;
		public Color color;
		private boolean[] overlap_list = new boolean[NMAX];
		
		public Body(double m, double rxn, double ryn, double vxn, double vyn) {
			this.m = m;
			this.radius = 4.0 * (1.0 + Math.log10(m));
			this.rxn = rxn;
			this.ryn = ryn;
			this.vxn = vxn;
			this.vyn = vyn;
			this.axn = 0.0;
			this.ayn = 0.0;
			Arrays.fill(overlap_list, false);
			
			if(Nbodies == 0) {
				this.color = Color.red;
			}
			else if(Nbodies < 512) {
				this.color = Color.blue;
			}
			else {
				this.color = Color.green;
			}
			
			this.id = Nbodies;
			Nbodies++;
			recalc_Plotting_reference = true;
			
			System.out.println("Creating body " + this.id + " with mass " + m + " at (" + rxn + ", " + ryn + ") with velocity (" + vxn + ", " + vyn + ")");
		}
		
		public static void addPotentials() {
			// Reset potential
			potential = 0.0;
			
			// Reset acceleration
			for(int i = 0; i < Nbodies; i++) {
				bodies[i].axn = 0.0;
				bodies[i].ayn = 0.0;
			}
			
			if(gravityOn) {
				for(int i = 0; i < Nbodies; i++) {
					for(int j = i + 1; j < Nbodies; j++) {
						bodies[i].addNewtonPotential(bodies[j]);
					}
				}
			}
			
			if(LJOn) {
				for(int i = 0; i < Nbodies; i++) {
					for(int j = i + 1; j < Nbodies; j++) {
						bodies[i].addLennardJonesPotential(bodies[j]);
					}
				}
			}
			
			if(uniformGOn) {
				for(int i = 0; i < Nbodies; i++) {
					final double g = 200.0;
					bodies[i].ayn += g;
					potential -= bodies[i].m * g * bodies[i].ryn;
				}
			}
		}
		
		public void addNewtonPotential(Body b) {
			final double dx = b.rxn - this.rxn;
			final double dy = b.ryn - this.ryn;
			final double r2 = dx * dx + dy * dy + eps2;
			final double r = Math.sqrt(r2);
			final double nx = dx / r;
			final double ny = dy / r;
			
			potential += -G * this.m * b.m / r;
			
			final double inv_sqr_x = G * nx / r2;
			final double inv_sqr_y = G * ny / r2;
			
			this.axn += inv_sqr_x * b.m;
			this.ayn += inv_sqr_y * b.m;
			b.axn -= inv_sqr_x * m;
			b.ayn -= inv_sqr_y * m;
		}
		
		public void addLennardJonesPotential(Body b) {
			final double dx = b.rxn - this.rxn;
			final double dy = b.ryn - this.ryn;
			//final double r2 = dx * dx + dy * dy + eps2;
			final double r2 = dx * dx + dy * dy;
			final double r6 = r2 * r2 * r2;
			final double r = Math.sqrt(r2);
			final double nx = dx / r;
			final double ny = dy / r;
			
			final double a_sig = this.radius;
			final double b_sig = b.radius;
			final double a_sig2 = a_sig * a_sig;
			final double b_sig2 = b_sig * b_sig;
			final double a_sig6 = a_sig2 * a_sig2 * a_sig2;
			final double b_sig6 = b_sig2 * b_sig2 * b_sig2;
			final double a_fac6 = a_sig6 / r6;
			final double b_fac6 = b_sig6 / r6;
			final double a_fac12 = a_fac6 * a_fac6;
			final double b_fac12 = b_fac6 * b_fac6;
			
			potential += LJ_mag * (a_fac12 - a_fac6);
			potential += LJ_mag * (b_fac12 - b_fac6);
			
			final double a_acc = -LJ_mag * (12.0 * b_fac12 - 6.0 * b_fac6) / (this.m * r);
			this.axn += nx * a_acc;
			this.ayn += ny * a_acc;
			
			final double b_acc = -LJ_mag * (12.0 * a_fac12 - 6.0 * a_fac6) / (b.m * r);
			b.axn -= nx * b_acc;
			b.ayn -= ny * b_acc;
		}
		
		public void elasticCollision(Body b) {
			double drx = b.rxn - this.rxn;
			double dry = b.ryn - this.ryn;
			double r = Math.sqrt(drx * drx + dry * dry);
			double rmin = (this.radius + b.radius) * 0.5;
			
			if(r < rmin && !overlap_list[b.id] && !b.overlap_list[this.id]) {			
				double dvx = b.vxn - this.vxn;
				double dvy = b.vyn - this.vyn;
				//double dv_dot_dr = dvx * drx + dvy * dry + eps2;
				double dv_dot_dr = dvx * drx + dvy * dry;
				double ir = 1.0 / r;
				double ir2 = ir * ir;
				double recoil = 2.0 * dv_dot_dr * ir2 / (this.m + b.m);				
				
				double newvxna = this.vxn + b.m * recoil * drx;
				double newvyna = this.vyn + b.m * recoil * dry;
				double newvxnb = b.vxn - this.m * recoil * drx;
				double newvynb = b.vyn - this.m * recoil * dry;
				
				this.vxn = newvxna;
				this.vyn = newvyna;
				b.vxn = newvxnb;
				b.vyn = newvynb;
				
				this.overlap_list[b.id] = true;
				b.overlap_list[this.id] = true; 
			}
			else if(r > rmin && (overlap_list[b.id] || b.overlap_list[this.id]) ) {
				this.overlap_list[b.id] = false;
				b.overlap_list[this.id] = false; 
			}
		}
		
		private static void Stormer_Verlet(final double time_step) {
			for(int i = 0; i < Nbodies; i++) {
				bodies[i].vxn += 0.5 * time_step * bodies[i].axn;
				bodies[i].vyn += 0.5 * time_step * bodies[i].ayn;
				bodies[i].rxn += time_step * bodies[i].vxn;
				bodies[i].ryn += time_step * bodies[i].vyn;
			}
			
			addPotentials();
			
			for(int i = 0; i < Nbodies; i++) {
				bodies[i].vxn += 0.5 * time_step * bodies[i].axn;
				bodies[i].vyn += 0.5 * time_step * bodies[i].ayn;
			}
			
		}
		
		private static void Stormer_Verlet_4() {
			final double a = 1.0 / (2.0 - Math.cbrt(2.0));
			final double b = 1.0 - 2.0 * a;
			Stormer_Verlet(a * dt);
			Stormer_Verlet(b * dt);
			Stormer_Verlet(a * dt);
		}
		
		private static void updateBodies() {
			if(elasticOn) {
				for(int i = 0; i < Nbodies; i++) {
					for(int j = 0; j < i; j++) {
						bodies[i].elasticCollision(bodies[j]);
					}
				}
			}
			
			if("Reflection".equals(boundary)) {
				for(int i = 0; i < Nbodies; i++) {
					double rad = 0.5 * bodies[i].radius;
					
					if(bodies[i].rxn < rad) {
						bodies[i].vxn = Math.abs(bodies[i].vxn);
						boundary_momentum_x += -2.0 * bodies[i].m * bodies[i].vxn;
					}
					else if(bodies[i].rxn > panelWidth - rad) {
						//System.out.println("Before vxn = " + bodies[i].vxn);
						
						bodies[i].vxn = -Math.abs(bodies[i].vxn);
						boundary_momentum_x += -2.0 * bodies[i].m * bodies[i].vxn;
						
						
					}
					
					if(bodies[i].ryn < rad) {
						bodies[i].vyn = Math.abs(bodies[i].vyn);
						boundary_momentum_y += -2.0 * bodies[i].m * bodies[i].vyn;
					}
					else if(bodies[i].ryn > panelHeight - rad) {
						bodies[i].vyn = -Math.abs(bodies[i].vyn);
						boundary_momentum_y += -2.0 * bodies[i].m * bodies[i].vyn;
					}
				}
			}
			else if("Torus".equals(boundary)) {
				for(int i = 0; i < Nbodies; i++) {
					if(bodies[i].rxn < 0) {
						bodies[i].rxn += panelWidth;
					}
					else if(bodies[i].rxn > panelWidth) {
						bodies[i].rxn -= panelWidth;
					}
					
					if(bodies[i].ryn < 0) {
						bodies[i].ryn += panelHeight;
					}
					else if(bodies[i].ryn > panelHeight) {
						bodies[i].ryn -= panelHeight;
					}
				}
			}
			else if("Sphere".equals(boundary)) {
				for(int i = 0; i < Nbodies; i++) {
					if(bodies[i].rxn < 0) {
						bodies[i].rxn += panelWidth;
					}
					else if(bodies[i].rxn > panelWidth) {
						bodies[i].rxn -= panelWidth;
					}
					
					if(bodies[i].ryn < 0) {
						bodies[i].rxn = (bodies[i].rxn + panelWidth / 2) % panelWidth;
						bodies[i].ryn = -bodies[i].ryn;
						bodies[i].vyn = -bodies[i].vyn;
					}
					else if(bodies[i].ryn > panelHeight) {
						bodies[i].rxn = (bodies[i].rxn + panelWidth / 2) % panelWidth;
						bodies[i].ryn = 2 * panelHeight - bodies[i].ryn; 
						bodies[i].vyn = -bodies[i].vyn;
					}
				}
			}
			
			if("Stormer-Verlet 4".equals(integrator)) {
				Stormer_Verlet_4();
			}
			else if("Stormer-Verlet 2".equals(integrator)) {
				Stormer_Verlet(dt);
			}
			
			double T = 0.0;
			angular_momentum = 0.0;
			linear_momentum_x = 0.0;
			linear_momentum_y = 0.0;
			for(int i = 0; i < Nbodies; i++) {
				T += bodies[i].m * (bodies[i].vxn * bodies[i].vxn + bodies[i].vyn * bodies[i].vyn);
				angular_momentum += bodies[i].m * (bodies[i].rxn * bodies[i].vyn - bodies[i].ryn * bodies[i].vxn);
				linear_momentum_x += bodies[i].m * bodies[i].vxn;
				linear_momentum_y += bodies[i].m * bodies[i].vyn;
			}
			T *= 0.5;
			
			linear_momentum_x += boundary_momentum_x;
			linear_momentum_y += boundary_momentum_y;
			
			Hamiltonian = T + potential;
			
			if(time < dt || recalc_Plotting_reference) {
				Plotting.reset_series = true;
				Plotting.reference_Hamiltonian = Hamiltonian;
				Plotting.reference_angular_momentum = angular_momentum;
				Plotting.reference_linear_momentum_x = linear_momentum_x;
				Plotting.reference_linear_momentum_y = linear_momentum_y;
				recalc_Plotting_reference = false;
			}
			
			time += dt;
			
			//System.out.println("potential = " + potential);
			//System.out.println("Hamiltonian = " + Hamiltonian);
			//System.out.println("linear_momentum_x = " + linear_momentum_x);
			//System.out.println("linear_momentum_y = " + linear_momentum_y);
			
//			System.out.format("t = %.2e, H = %.8e%n", time, Hamiltonian);
//			for(int i = 0; i < Nbodies; i++) {
//				System.out.println("bodies[" + i + "].vxn = " + bodies[i].vxn + ", vyn = " + bodies[i].vyn);
//			}
		}
		
		public static void setCOMFrame() {
			recalc_Plotting_reference = true;
			
			double com_rx = 0.0, com_ry = 0.0;
			double com_vx = 0.0, com_vy = 0.0;
			double total_m = 0.0;
			
			for(int i = 0; i < Nbodies; i++) {
				com_rx += bodies[i].m * bodies[i].rxn;
				com_ry += bodies[i].m * bodies[i].ryn;
				com_vx += bodies[i].m * bodies[i].vxn;
				com_vy += bodies[i].m * bodies[i].vyn;
				total_m += bodies[i].m;
			}
			
			com_rx /= total_m;
			com_ry /= total_m;
			com_vx /= total_m;
			com_vy /= total_m;
			
			for(int i = 0; i < Nbodies; i++) {
				bodies[i].rxn -= com_rx - 0.5 * panelWidth;
				bodies[i].ryn -= com_ry - 0.5 * panelHeight;
				bodies[i].vxn -= com_vx;
				bodies[i].vyn -= com_vy;
			}
		}
		
		public static void resetBodies() {
			for(int i = 0; i < Nbodies; i++) {
				bodies[i] = null;
			}
			
			Nbodies = 0;
			time = 0.0;
			Hamiltonian = 0.0;
			angular_momentum = 0.0;
			boundary_momentum_x = 0.0;
			boundary_momentum_y = 0.0;
			Plotting.reference_Hamiltonian = 0.0;
			Plotting.reference_linear_momentum_x = 0.0;
			Plotting.reference_linear_momentum_y = 0.0;
			Plotting.reference_angular_momentum = 0.0;
			recalc_Plotting_reference = true;
		}
		
		public static void loadBody(final double m, final double rx, final double ry, final double vx, final double vy) {
			if(Nbodies < NMAX) {
				bodies[Nbodies] = new Body(m, rx, ry, vx, vy);
			}
		}
		
	}
}
