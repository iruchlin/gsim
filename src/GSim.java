import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class GSim extends JFrame {
	
	public GSim() {
		System.out.println("Starting GSim...");
		setResizable(false);
		setIconImage(new ImageIcon("ico/calG_icon.png").getImage());
		setTitle("GSim");
		setVisible(true);
		//setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		
		add(new Control());
		add(new Stage());
		add(new Plotting());
		pack();
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
            
            @Override
            public void run() {                
            	new GSim();
            }
        });
	}
	
}
